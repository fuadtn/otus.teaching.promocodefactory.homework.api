﻿using Microsoft.AspNetCore.SignalR.Client;


var connection = new HubConnectionBuilder()
    .WithUrl("https://localhost:5001/CustomerHub")
    .Build();

try
{
    connection.StartAsync().Wait();
    //connection.InvokeCoreAsync("Send", args: new[] { "hello from client"} );
    connection.On("Notify", (string message) =>
        Console.WriteLine(message)
    );
    Console.ReadKey();
}
finally
{
    connection.StopAsync().Wait();
}
